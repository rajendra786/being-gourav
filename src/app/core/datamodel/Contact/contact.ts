export interface Contact{
    icon:string;
    title:string;
    description:string;
}