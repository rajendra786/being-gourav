import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import{InMemoryWebApiModule} from 'angular-in-memory-web-api';
import { HeaderComponent } from './core/components/header/header.component';
import { FooterComponent } from './core/components/footer/footer.component';
import { from } from 'rxjs';
import { HomeData } from './core/mock/homedata';
import { HomeService } from './core/services/Home_service/home.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
@NgModule({
  declarations: [AppComponent,  
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    InMemoryWebApiModule.forRoot(HomeData),
    HttpClientModule,
    CommonModule
   ],
  providers: [HomeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
