export interface Topstory{
    heading:string;
    img:string;
    title:string;
    description:string;
}