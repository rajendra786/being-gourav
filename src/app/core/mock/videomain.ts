import{InMemoryDbService} from 'angular-in-memory-web-api';
export class VideoData implements InMemoryDbService{
    createDb(){
        let videomain=[
            {
                icon:'fa fa-graduation-cap red-1',
                title:'Photoshop',
                description:'A Study Platform where Students can explore their knowledge.',
                link:'Read More'
            },
            {
                icon:'fa fa-leanpub red-1',
                title:'HTML',
                description:'A virtual Training approach to provide a solution anytime.',
                link:'Read More'
            },
            {
                icon:'fa fa-television red-1',
                title:'CSS',
                description:'We build Websites and provide SEO services to new &amp; existing Websites.',
                link:'Read More'
            },
            {
                icon:'fa fa-pencil-square-o red-1',
                title:'Jquery',
                description:'A productive Content, which leads to an appropriate Information.',
                link:'Read More'
            },
            {
                icon:'fa fa-television red-1',
                title:'CSS',
                description:'We build Websites and provide SEO services to new &amp; existing Websites.',
                link:'Read More'
            },
            {
                icon:'fa fa-pencil-square-o red-1',
                title:'Jquery',
                description:'A productive Content, which leads to an appropriate Information.',
                link:'Read More'
            }
           
        ];
        let videocourse=[
            {
                icon:'fa fa-map-marker',
                title:'Web Design',
                description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
                link:'Read More',
                licon:'fa fa-angle-right'
              },
              {
                icon:'fa fa-map-marker',
                title:'Web Design',
                description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
                link:'Read More',
                licon:'fa fa-angle-right'
              },
              {
                icon:'fa fa-map-marker',
                title:'Web Design',
                description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
                link:'Read More',
                licon:'fa fa-angle-right'
              }
        ];
        return{ videomain,videocourse};
    }
}