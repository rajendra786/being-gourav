import{InMemoryDbService} from 'angular-in-memory-web-api';
export class VideoData implements InMemoryDbService{
    createDb(){
        let video=[
           {
             icon:'fa fa-map-marker',
             title:'Web Design',
             description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
             link:'Read More',
             licon:'fa fa-angle-right'
           },
           {
            icon:'fa fa-phone-square',
            title:'Web Devlopment',
            description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
            link:'Read More',
            licon:'fa fa-angle-right'
          },
          {
            icon:'fa fa-envelope',
            title:'Networking',
            description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
            link:'Read More',
            licon:'fa fa-angle-right'
          },
          {
            icon:'fa fa-map-marker',
            title:'Web Design',
            description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
            link:'Read More',
            licon:'fa fa-angle-right'
          },
          {
           icon:'fa fa-phone-square',
           title:'Web Devlopment',
           description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
           link:'Read More',
           licon:'fa fa-angle-right'
         },
         {
           icon:'fa fa-envelope',
           title:'Networking',
           description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
           link:'Read More',
           licon:'fa fa-angle-right'
         },
         {
            icon:'fa fa-phone-square',
            title:'Web Devlopment',
            description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
            link:'Read More',
            licon:'fa fa-angle-right'
          },
          {
            icon:'fa fa-envelope',
            title:'Networking',
            description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.',
            link:'Read More',
            licon:'fa fa-angle-right'
          },
        ];
        return {video};
    }
}