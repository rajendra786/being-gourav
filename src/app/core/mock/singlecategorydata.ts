import{InMemoryDbService} from 'angular-in-memory-web-api';
export class SinglecategoryData implements InMemoryDbService{
    createDb(){
       let review=[
           {
            tName:'Student Name',
            tdate:'July 7, 2020',
            icon:'fa fa-star',
            icon1:'fa fa-star',
            icon2:'fa fa-star',
            icon3:'fa fa-star-half-o',
            icon4:'fa fa-star-o',
            description:'learn how to approach a programming problem methodically, so you can formulate an algorithm that is specific and correct. You will work through examples with sequences of numbers and graphical patterns to develop the skill of algorithm development.'
           },
           {
            tName:'Student Name',
            tdate:'July 7, 2020',
            icon:'fa fa-star',
            icon1:'fa fa-star',
            icon2:'fa fa-star',
            icon3:'fa fa-star-half-o',
            icon4:'fa fa-star-o',
            description:'learn how to approach a programming problem methodically, so you can formulate an algorithm that is specific and correct. You will work through examples with sequences of numbers and graphical patterns to develop the skill of algorithm development.'
           }
       ];
       let htmlvideo=[
        {
            icon:'fa fa-play-circle',
            description:'learn how to approach a programming problem methodically',
            img:'assets/images/t1.jpg',
            span:'By',
            p:'Being Gourav',
            span1:'Date',
            p1:'17-07-2020',
            link:'see more',
            licon:'fa fa-angle-right'
        },
        {
                icon:'fa fa-play-circle',
                description:'learn how to approach a programming problem methodically',
                img:'assets/images/t1.jpg',
                span:'By',
                p:'Being Gourav',
                span1:'Date',
                p1:'17-07-2020',
                link:'see more',
                licon:'fa fa-angle-right'
         },
         {
                icon:'fa fa-play-circle',
                description:'learn how to approach a programming problem methodically',
                img:'assets/images/t1.jpg',
                span:'By',
                p:'Being Gourav',
                span1:'Date',
                p1:'17-07-2020',
                link:'see more',
                licon:'fa fa-angle-right'
           },
            {
                    icon:'fa fa-play-circle',
                    description:'learn how to approach a programming problem methodically',
                    img:'assets/images/t1.jpg',
                    span:'By',
                    p:'Being Gourav',
                    span1:'Date',
                    p1:'17-07-2020',
                    link:'see more',
                    licon:'fa fa-angle-right'
                },
                {
                    icon:'fa fa-play-circle',
                    description:'learn how to approach a programming problem methodically',
                    img:'assets/images/t1.jpg',
                    span:'By',
                    p:'Being Gourav',
                    span1:'Date',
                    p1:'17-07-2020',
                    link:'see more',
                    licon:'fa fa-angle-right'
                },
                {
                        icon:'fa fa-play-circle',
                        description:'learn how to approach a programming problem methodically',
                        img:'assets/images/t1.jpg',
                        span:'By',
                        p:'Being Gourav',
                        span1:'Date',
                        p1:'17-07-2020',
                        link:'see more',
                        licon:'fa fa-angle-right'
                    },
                    {
                        icon:'fa fa-play-circle',
                        description:'learn how to approach a programming problem methodically',
                        img:'assets/images/t1.jpg',
                        span:'By',
                        p:'Being Gourav',
                        span1:'Date',
                        p1:'17-07-2020',
                        link:'see more',
                        licon:'fa fa-angle-right'
                    },
                    {
                            icon:'fa fa-play-circle',
                            description:'learn how to approach a programming problem methodically',
                            img:'assets/images/t1.jpg',
                            span:'By',
                            p:'Being Gourav',
                            span1:'Date',
                            p1:'17-07-2020',
                            link:'see more',
                            licon:'fa fa-angle-right'
                        }
    ];
        return{review,htmlvideo}
    }
}