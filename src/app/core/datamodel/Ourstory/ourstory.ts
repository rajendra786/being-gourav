import{Topstory} from './topstory';

export interface Ourstory {
    img:string;
    title:string;
    subtitle:string;
    description:string;
    topStory:Topstory[];
}