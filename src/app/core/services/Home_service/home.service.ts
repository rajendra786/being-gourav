import { Injectable } from '@angular/core';
import { Carosel } from '../../datamodel/Home/carosel';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  caroselurl='api/carosel'
  constructor(private http:HttpClient) { }
  getcarosel():Observable<Carosel[]>{
    return this.http.get<Carosel[]>(this.caroselurl);
  }
}
