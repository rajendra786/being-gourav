export interface Ournews{
    img:string;
    heading:string;
    description:string;
    link:string;
    icon:string;
}