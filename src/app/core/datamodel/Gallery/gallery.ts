export interface Gallery{
    img:string;
    icon:string;
    title:string;
}