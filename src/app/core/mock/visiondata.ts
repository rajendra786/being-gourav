import{InMemoryDbService} from 'angular-in-memory-web-api';
export class VisionData implements InMemoryDbService{
    createDb(){
        let vision1={
            title:'What Else Do I Need To Keep In Mind?',
            description1:'If your dry product weight has different size options, you need to consider how dense of a product you have. The more dense your product is, the smaller the container it will fit in.',
            description2:'Likewise, the less dense your product is, the larger the container you will need for it. Another rule of thumb is that the cleaner the grower of the product, the less dense your product will be.',
            img:'assets/images/image-5.jpg',
            img1:'assets/images/image-6.jpg',
            link:'https://www.youtube.com/watch?v=e_WOEL6F1YE',
            icon:'fa fa-thumbs-up'
        }
        let vision2={
            img:'assets/images/image-5.jpg',
            img1:'assets/images/image-6.jpg',
            link:'https://www.youtube.com/watch?v=e_WOEL6F1YE',
            icon:'fa fa-thumbs-up',
            title:'What Else Do I Need To Keep In Mind?',
            description1:'If your dry product weight has different size options, you need to consider how dense of a product you have. The more dense your product is, the smaller the container it will fit in.',
            description2:'Likewise, the less dense your product is, the larger the container you will need for it. Another rule of thumb is that the cleaner the grower of the product, the less dense your product will be.',
        }
        return {vision1,vision2}
    }
}