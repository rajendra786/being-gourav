import {InMemoryDbService} from 'angular-in-memory-web-api';
export class OurstoryData implements InMemoryDbService{
    createDb(){
        let ourstory={
            img:'assets/images/storyimg.jpg',
            title:'Our',
            subtitle:'Story',
            description:'If your dry product weight has different size options, you need to consider how dense of a product you have. The more dense your product is, the smaller the container it will fit in.',
            topStory:[
                {  
                    heading:'If your dry product weight has different size options, you need to consider how dense of a product you have. The more dense your product is, the smaller the container it will fit in.',
                    img:'assets/images/icon12.png',
                    title:'Lorem ipsum dolor sit',
                    description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, doloribus, accusamus. Commodi voluptas,'
                },
                {   
                   heading:'If your dry product weight has different size options, you need to consider how dense of a product you have. The more dense your product is, the smaller the container it will fit in.',
                   img:'assets/images/icon12.png',
                   title:'Lorem ipsum dolor sit',
                   description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, doloribus, accusamus. Commodi voluptas,'
               },
               {
                   heading:'If your dry product weight has different size options, you need to consider how dense of a product you have. The more dense your product is, the smaller the container it will fit in.',
                   img:'assets/images/icon12.png',
                   title:'Lorem ipsum dolor sit',
                   description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, doloribus, accusamus. Commodi voluptas,'
              }
            ]
        };
        return{ourstory};
    }
}