export interface Latest{
    img:string;
    title:string;
    icon:string;
    description:string;
}