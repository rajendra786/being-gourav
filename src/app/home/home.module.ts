import { NgModule } from '@angular/core';


import { HomeRoutingModule } from './home-routing.module';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [HomeLayoutComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
