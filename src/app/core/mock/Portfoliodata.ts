import{InMemoryDbService} from 'angular-in-memory-web-api';
export class PortfolioData implements InMemoryDbService{
    createDb(){
        let portfolio=[
            {
                img:'assets/images/portimg3.png',
                icon:'fa fa-picture-o GalICon',
                title:'Lorem ipsum'
            },
            {
                img:'assets/images/portimg1.png',
                icon:'fa fa-picture-o GalICon',
                title:'Lorem ipsum'
            },
            {
                img:'assets/images/portimg1.png',
                icon:'fa fa-picture-o GalICon',
                title:'Lorem ipsum'
            }
        ];
        return {portfolio};
    }
}