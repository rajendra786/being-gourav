export interface Blog{
    img:string;
    user:string;
    ut:string;
    ud:string;
    comment:string;
    cd:string;
    celender:string;
    cdes:string;
    title:string;
    description:string;
    link:string;
    licon:string;
}