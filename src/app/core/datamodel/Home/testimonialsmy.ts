export interface Testimonial{
    description:string;
    img:string;
    heading:string;
    subheading:string;
}