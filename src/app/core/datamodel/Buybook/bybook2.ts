export interface Bybook2{
    img:string;
    title:string;
    subtitle:string;
    description:string;
    span:string;
    link:string;
}