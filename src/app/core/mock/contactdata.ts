import{InMemoryDbService} from 'angular-in-memory-web-api';
export class ContactData implements InMemoryDbService{
    createDb(){
        let contact=[
            {
                icon:'fa fa-map-marker',
                title:'Address',
                description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era.'
            },
            {
                icon:'fa fa-phone-square',
                title:'Contact Us',
                description:'1234567890'
            },
            {
                icon:'fa fa-envelope',
                title:'Mail Us',
                description:'demo#gmail.com'
            }
        ];
        return {contact};
    }
}