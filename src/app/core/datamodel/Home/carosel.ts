export interface Carosel{
    background:string;
    title:string;
    description:string;
    button:string;
    icon:string;
    img:string;
}