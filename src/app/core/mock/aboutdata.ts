import {InMemoryDbService} from 'angular-in-memory-web-api';
export class AboutData implements InMemoryDbService{
    createDb(){
        let gourav={
          img:'assets/images/about-1.jpg',
          title:'Mr. Gourav Manjrekar',
          subtitle:'Founder',
          description:'Mr. Gourav Manjrekar is the Founder of BeingGourav.com and is looking forward to provide the best IT experience to its students, users, and clients. He has expertise in the field of Statistics, Mathematics, and Operations Research. He is facilitated with 2 Gold Medals in M.SC (Statistics) in 2011 from DAVV, Indore. He has teaching experience of more than 10 years and has been a resource person in various Workshops and Seminars. At present students are also taking advantage of best learning experience from  his Youtube Channel and other Social Media platforms.'
        };
        let swapnil={
            title:'Mr. Swapnil Girase',
            subtitle:'Co-Founder',
            description:'Mr. Swapnil Girase is the Co-founder of BeingGourav.com, a young entrepreneur from Indore whose mission is to provide quality Web and Software Development solutions in this competitive fierce marketplace.  He has more than 4 years of experience in the IT industry while working as an SAP Consultant along with R/3 and R/2 architecture. He completed his Masters in Computer Applications (MCA) in 2016 from RGPV and at present is all set to make BeingGourav.com a leading organization and provide the best services to its students, users, and clients.',
            img:'assets/images/about-2.jpg'
        };
        let anil={
            img:'assets/images/about-1.jpg',
            title:'Mr. Anil Kumar',
            subtitle:'Co-Founder',
            description:'Mr. Anil Kumar is Co-founder of BeingGourav.com. He has his excellence in IT Services and directs all the development projects that are instrumental in driving benefits to the firm. He has more than 4 years of experience in his field and is a Full Stack Developer. He has commendable knowledge of computer languages, Backend, Databases, Front-End, Ui/UX etc. At present he is exercising his skills in PYTHON DJANGO, VUEJS, PHOTOSHOP, MONGODB and many such programs and languages.'
        };
        let ourteam=[
            {
                img:'assets/images/team-member-01.jpg',
                title:'Kemel D. Dimola',
                subtitle:'Lorem Ipsum Doller',
                facebook:'fa fa-facebook-f',
                twitter:'fa fa-twitter',
                msg:'fa fa-envelope-o',
                linkedin:'fa fa-linkedin'
            },
            {
                img:'assets/images/team-member-04.jpg',
                title:'Kemel D. Dimola',
                subtitle:'Lorem Ipsum Doller',
                facebook:'fa fa-facebook-f',
                twitter:'fa fa-twitter',
                msg:'fa fa-envelope-o',
                linkedin:'fa fa-linkedin'
            },
            {
                img:'assets/images/team-member-01.jpg',
                title:'Kemel D. Dimola',
                subtitle:'Lorem Ipsum Doller',
                facebook:'fa fa-facebook-f',
                twitter:'fa fa-twitter',
                msg:'fa fa-envelope-o',
                linkedin:'fa fa-linkedin'
            }
        ];
        return {gourav,swapnil,anil,ourteam};
    }
}