export interface Ourteam{
    img:string;
    title:string;
    subtitle:string;
    facebook:string;
    twitter:string;
    msg:string;
    linkedin:string;
}