import { Component, OnInit } from '@angular/core';

import { HomeService } from 'src/app/core/services/Home_service/home.service';
import { Carosel } from 'src/app/core/datamodel/Home/carosel';
declare var $:any;
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit{
 slider:Carosel[];
 slide:any=[
    {
        background:'assets/images/slider-home-start-ups-slide-01-bg.png',
        title:'result-driven creative agency.',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        button:'Read More',
        icon:'fa fa-angle-right',
        img:'assets/images/banner1.png'
       },
       {
        background:'assets/images/slider-home-start-ups-slide-01-bg.png',
        title:'result-driven creative agency.',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        button:'Read More',
        icon:'fa fa-angle-right',
        img:'assets/images/banner2.png'
       },
       {
        background:'assets/images/slider-home-start-ups-slide-01-bg.png',
        title:'result-driven creative agency.',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        button:'Read More',
        icon:'fa fa-angle-right',
        img:'assets/images/banner3.png'
       }
 ];
  constructor(private home:HomeService) { 
    this.getcarosel();
  }
  ngOnInit(): void {
    $('.main-banner').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      transitionStyle : "fade",
      responsive:{
         0:{
             items:1
         },
         600:{
             items:1
         },
         1000:{
             items:1
         }
      }, navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
      })
      
      $('#testimonialsmy').owlCarousel({
      loop:true,
      margin:0,
      nav:true,
      autoplay:true, 
      autoplayTimeout:10000, // time for slides changes
      smartSpeed: 1000, // duration of change of 1 slide
      responsive:{
      0:{
       items:1
       },
       600:{
       items:2
       },
       1000:{
       items:1
      }
      }
      })

      $('.count').each(function () {
         $(this).prop('Counter',0).animate({
             Counter: $(this).text()
         }, {
             duration: 4000,
             easing: 'swing',
             step: function (now) {
                 $(this).text(Math.ceil(now));
             }
         });
          });
      }
  getcarosel()
   {
    this.home.getcarosel().subscribe( data =>this.slider=data );
   }
}
