export interface Bybook1{
    img:string;
    title:string;
    subtitle:string;
    description:string;
    span:string;
    link:string;
}