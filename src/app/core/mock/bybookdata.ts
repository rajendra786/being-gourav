import {InMemoryDbService} from 'angular-in-memory-web-api';
export class BybookData implements InMemoryDbService{
    createDb(){
        let bybook1={
            img:'assets/images/bookmg1.jpg',
            title:'Being Gaurav',
            subtitle:'Books',
            description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, doloribus, accusamus. Commodi voluptas, aliquam corporis dolorem amet? Asperiores ullam consequatur dolores.',
            span:'$100.00',
            link:'Buy now'
        }
        let bybook2={
            img:'assets/images/bookimg2.jpg',
            title:'Being Gaurav',
            subtitle:'Books',
            description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, doloribus, accusamus. Commodi voluptas, aliquam corporis dolorem amet? Asperiores ullam consequatur dolores.',
            span:'$100.00',
            link:'Buy now'
           
        }
        return{bybook1,bybook2}
    }
}