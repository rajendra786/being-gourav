import{InMemoryDbService} from 'angular-in-memory-web-api';
export class BlogData implements InMemoryDbService{
    createDb(){
       let blog=[
     {
        img:'assets/images/b7.jpg',
        user:'fa fa-user',
        ut:'posted by',
        ud:'Blog Themes',
        comment:'fa fa-comments',
        cd:'5 Comments',
        celender:'fa fa-calendar',
        cdes:'January 04, 2016',
        title:'Pellentesque habitant morbi',
        description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et mi vulputate gen vehicula maximus sagittis rhoncus tortor. Class aptent taciti sociosqu ad litora torquent perconubia nostra, per inceptos himenaeos. Sed vel nisi orci. Pellentesque sed dignissim dolor, sit amet gravida orci. Suspendisse imperdiet ex vel lacus imperdiet mollis. Sed feugiat enim sed eros interdum, […]',
        link:'Read More',
        licon:'fa fa-angle-right'
       },
       {
        img:'assets/images/b5.jpg',
        user:'fa fa-user',
        ut:'posted by',
        ud:'Blog Themes',
        comment:'fa fa-comments',
        cd:'5 Comments',
        celender:'fa fa-calendar',
        cdes:'January 04, 2016',
        title:'Pellentesque habitant morbi',
        description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et mi vulputate gen vehicula maximus sagittis rhoncus tortor. Class aptent taciti sociosqu ad litora torquent perconubia nostra, per inceptos himenaeos. Sed vel nisi orci. Pellentesque sed dignissim dolor, sit amet gravida orci. Suspendisse imperdiet ex vel lacus imperdiet mollis. Sed feugiat enim sed eros interdum, […]',
        link:'Read More',
        licon:'fa fa-angle-right'
       },
       {
        img:'assets/images/b7.jpg',
        user:'fa fa-user',
        ut:'posted by',
        ud:'Blog Themes',
        comment:'fa fa-comments',
        cd:'5 Comments',
        celender:'fa fa-calendar',
        cdes:'January 04, 2016',
        title:'Pellentesque habitant morbi',
        description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et mi vulputate gen vehicula maximus sagittis rhoncus tortor. Class aptent taciti sociosqu ad litora torquent perconubia nostra, per inceptos himenaeos. Sed vel nisi orci. Pellentesque sed dignissim dolor, sit amet gravida orci. Suspendisse imperdiet ex vel lacus imperdiet mollis. Sed feugiat enim sed eros interdum, […]',
        link:'Read More',
        licon:'fa fa-angle-right'
       },
       {
        img:'assets/images/b5.jpg',
        user:'fa fa-user',
        ut:'posted by',
        ud:'Blog Themes',
        comment:'fa fa-comments',
        cd:'5 Comments',
        celender:'fa fa-calendar',
        cdes:'January 04, 2016',
        title:'Pellentesque habitant morbi',
        description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et mi vulputate gen vehicula maximus sagittis rhoncus tortor. Class aptent taciti sociosqu ad litora torquent perconubia nostra, per inceptos himenaeos. Sed vel nisi orci. Pellentesque sed dignissim dolor, sit amet gravida orci. Suspendisse imperdiet ex vel lacus imperdiet mollis. Sed feugiat enim sed eros interdum, […]',
        link:'Read More',
        licon:'fa fa-angle-right'
       }
    ];
    let blogcategory=[
        {
            icon:'fa fa-caret-right',
            title:'Business'
        },
        {
            icon:'fa fa-caret-right',
            title:'Buying Properties'
        },
        {
            icon:'fa fa-caret-right',
            title:'Construction'
        },
        {
            icon:'fa fa-caret-right',
            title:'Location'
        },
        {
            icon:'fa fa-caret-right',
            title:'Real Estate'
        }
    ];
    let letest=[
        {
            img:'assets/images/b7.jpg',
            title:'6 Questions to ask sublet your NYC',
            icon:'fa fa-calendar-o',
            description:'October, 24 2019'
        },
        {
            img:'assets/images/b5.jpg',
            title:'6 Questions to ask sublet your NYC',
            icon:'fa fa-calendar-o',
            description:'October, 24 2019'
        },
        {
            img:'assets/images/b6.jpg',
            title:'6 Questions to ask sublet your NYC',
            icon:'fa fa-calendar-o',
            description:'October, 24 2019'
        },
    ];
    
    let social=[
        {
            link:'https://www.facebook.com/BeingGouravETITS/',
            img:'assets/images/social-facebook.jpg'
        },
        {
            link:'https://twitter.com/beinggourav_com',
            img:'assets/images/social-twitter.jpg'
        },
        {
            link:'https://www.linkedin.com/company/beinggourav/?viewAsMember=true',
            img:'assets/images/social-linkedin.jpg'
        },
        {
            link:'https://www.instagram.com/beinggourav/',
            img:'assets/images/insta.jpg'
        },
        {
            link:'https://www.youtube.com/channel/UCUTlgKrzGsIaYR-Hp0RplxQ?app=deskto',
            img:'assets/images/social-youtube.png'
        }

    ];
   let  blogImage=[
           {
               img:'assets/images/b7.jpg'
           },
           {
            img:'assets/images/b6.jpg'
        },
        {
            img:'assets/images/b5.jpg'
        },
        {
            img:'assets/images/b7.jpg'
        },
        {
            img:'assets/images/b6.jpg'
        },
        {
            img:'assets/images/b5.jpg'
        },
        {
            img:'assets/images/b7.jpg'
        }
   ];
           return {blog,letest,social,blogImage}
    }
}