export interface TopThree{
    img:string;
    title:string;
    description:string;
    button:string;
    icon:string;
}
