export interface Videomain{
    icon:string;
    title:string;
    description:string;
    link:string;
}