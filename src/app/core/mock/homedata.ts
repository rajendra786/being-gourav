import { InMemoryDbService } from 'angular-in-memory-web-api';
export class HomeData implements InMemoryDbService{
createDb(){
    let carosel=[
       {
        background:'assets/images/slider-home-start-ups-slide-01-bg.png',
        title:'result-driven creative agency.',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        button:'Read More',
        icon:'fa fa-angle-right',
        img:'assets/images/banner1.png'
       },
       {
        background:'assets/images/slider-home-start-ups-slide-01-bg.png',
        title:'result-driven creative agency.',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        button:'Read More',
        icon:'fa fa-angle-right',
        img:'assets/images/banner2.png'
       },
       {
        background:'assets/images/slider-home-start-ups-slide-01-bg.png',
        title:'result-driven creative agency.',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        button:'Read More',
        icon:'fa fa-angle-right',
        img:'assets/images/banner3.png'
       }
    ];
   let topthree=[
     {
       img:'assets/images/creative-blueactive.png',
       title:'Branding Consult',
       description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era',
       button:'Read More',
       icon:'fa fa-angle-right'
     },
     {
      img:'assets/images/creative-blue.png',
      title:'Branding Consult',
      description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era',
      button:'Read More',
      icon:'fa fa-angle-right'
    },
    {
      img:'assets/images/creative-blue.png',
      title:'Branding Consult',
      description:'Aenean lacinia bibendum nulla sed consectetur. Integer posuere era',
      button:'Read More',
      icon:'fa fa-angle-right'
    }
    ];
    let ourservice=[
    {
      icon:'fa fa-graduation-cap red-1',
      title:'Education',
      description:'A Study Platform where Students can explore their knowledge.'
    },
    {
      icon:'fa fa-leanpub red-1',
      title:'Online Learning',
      description:'A virtual Training approach to provide a solution anytime.'
    },
    {
      icon:'fa fa-television red-1',
      title:'Web Development & SEO',
      description:'We build Websites and provide SEO services to new & existing Websites.'
    },
    {
      icon:'fa fa-pencil-square-o red-1',
      title:'Blog Services',
      description:'A page of productive Content, which leads to an appropriate Information.'
    },
    {
      icon:'fa fa-book red-1',
      title:'Hosting Services',
      description:'We deliver Hosting Services at a very minimal price.'
    },
    {
      icon:'fa fa-television red-1',
      title:'Content Writing',
      description:'We Offer Content writing Services as per your requirement.'
    },
    {
      icon:'fa fa-television red-1',
      title:'Entertnment',
      description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
    },
    {
      icon:'fa fa-television red-1',
      title:'Entertnment',
      description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
     }
  ];
  let counterbox=[
    {
      img:'assets/images/teacher.png',
      title:'3',
      description:'Certified Teachers'
    },
    {
      img:'assets/images/time-and-date.png',
      title:'10000',
      description:'Subscribers'
    },
    {
      img:'assets/images/book.png',
      title:'4',
      description:'Approved Subjects'
    },
    {
      img:'assets/images/positive-comment.png',
      title:'100000',
      description:'Happy Students'
    }
  ];
  let testimonial=[
    {
      description:'This Website is Awesome...',
      img:'assets/images/client.jpg',
      heading:'Anil Kumar',
      subheading:'Developer'
    },
    {
      description:'Technology has made it easier for students to learn with devices new, but nothing can come close to the experience of being taught by an inspirational teacher like you. Thank you so much Gourav Sir.',
      img:'assets/images/client.jpg',
      heading:'Akshay Saxena',
      subheading:'Happy Student'
    },
    {
      description:'To the world you may be just a teacher but to your student you are a hero". Thank you Gourav Sir... ',
      img:'assets/images/client.jpg',
      heading:'Deeksha Bundela',
      subheading:'Happy Student'
    }
  ];
  let ournews=[
    {
    img:'assets/images/news_img1.png',
    heading:'Lorem Ipsum is simply dummy text of the printing and typesetting industry?',
    description:'Lorem Ipsum is simply dummy text of  industry Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read More',
    icon:'fa fa-angle-right'
    },
    {
      img:'assets/images/news_img2.png',
      heading:'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      description:'Lorem Ipsum is simply dummy text of  typesetting.industry Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      link:'Read More',
      icon:'fa fa-angle-right'
    }
  ];
    return {carosel,topthree,ourservice,counterbox,testimonial,ournews
    };
  }
}